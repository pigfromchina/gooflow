# GooFlow ![release](https://img.shields.io/github/release/sdlddr/Gooflow.svg?style=flat-square) ![rop-size](https://img.shields.io/github/repo-size/sdlddr/Gooflow.svg?style=flat-square&colorB=ff6600)

> 这是一个用来在 Web 端构建流程图的 JQuery 插件，在线流程设计器。可设计各种流程图、逻辑流图，数据流图，或是应用系统中需要走流程的功能实现。优秀的用户体验使得操作界面很容易上手，无论开者或用户都可轻松使用。并且兼容主流浏览器(IE8 - Edge，Chrome，Firefox)。

官网：[gooflow.xyz](https://gooflow.xyz)

要了解详细的使用方法，请查看 [API 文档](http://gooflow.xyz/docs)

## 授权说明：

当前托管项目中所放版本仅为试用版，__内含挖矿程序，只能用于本地功能测试，切不可放入实际项目中使用；如要使用纯净未有任何附加的正式版，需要联系作者本人。__

__本组件正式版可免费用于非营利性项目中__；如果要使用在以 _营利为目的软件开发项目中_，__请向作者购买商业使用授权，谢谢__。_（如果逃避购买授权，擅自窃取用作谋利，即属严重侵权行为，与盗窃无异。产生的一切任何后果责任由侵权者自负。请各位只想免费吃白饭的伸手党做好向作者本人及自己的公司、客户负全责的觉悟。）_

联系方式：

+ 微信：18648945414
+ QQ：115247126
+ 支付宝：[fool-egg@163.com](mailto:fool-egg@163.com)

## 特点

+ 跨领域：流程图设计器不止用在电信领域，在其它需要 IT 进行技术支持的领域中都有重大作用
+ _页面顶部栏、左边侧边栏_ 均可自定义
+ _当左边的侧边栏设为不显示时_，为 _只读_ 状态，此时的视图区可当作是一个 _查看器_ 而非 _编辑器_
+ 当前最新版本已全部使用自定义的 _阿里巴巴矢量图标库_，不再需要用来 _显示图标样式的图片_
+ 侧边工具栏除了基本和一些流程节点按钮外，还自定义新的 _节点按钮_，自定义节点都可以有自有的图标、类型名称，定义后在使用可可在工作区内增加这些自定义节点
+ 顶部栏可显示流程图数据组的标题，也可提供一些常用操作按钮
+ 顶部栏的按钮，除了 _撤销、重做_ 按钮外，其余按钮均可 _自定义点击事件_
+ 可画 _直线、折线_，折线还可以左右/上下移动其中段
+ 具有 __区域划分__ 功能，能让用户更直观地了解哪些节点及其相互间的转换，是属于何种自定义区域内的
+ 具有 __标注__ 功能，用橙红色标注某个结点或者转换线，一般用在展示流程进度时
+ 能直接 _双击结点、连线、分组区域_ 中的文字进行编辑
+ 在 _对结点、连线、分组区域_ 的各种编辑操作，如 _新增/删除/修改名称/重设样式或大小/移动/标注_ 时，均可捕捉到事件，并触发自定义事件，如果自定义事件执行的方法返回 `false`，则会阻止操作
+ 具有 __操作事务序列控制功能__，在工作区内的各种 _有效操作都能记录到一个栈中_，然后可以进行 __撤销__ `undo()` 或 __重做__ `redo()`，像典型的 _C/S_ 软件一样
+ 能将流程图以 png 图片的格式 _导出并下载_（纯 JS 实现，但 __不支持 IE9 及以下浏览器__）

![Preview Image](https://git.oschina.net/uploads/images/2017/0531/145320_f0bb8c2c_472359.png "效果预览图")

## 开始使用

### 一、Node.js 环境下的安装：

checkout 本项目后，在项目所在目录先运行：

```bash
node.js
npm install
```

再运行：

```bash
node.js
npm run build
```

即可得到运行 Demo 所需要的 GooFlow 发布版文件。

### 二、传统方式的使用方法

先在页头引入 CSS 文件，在 `body` 末尾引入 JQuery 和 GooFlow 主要功能文件：

```html
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="./dist/GooFlow.min.css"/>
    </head>
    <body>
        <div id="demo"></div>
        <!-- …… -->
        <script type="text/javascript" src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min"></script>
        <script type="text/javascript" src="./dist/GooFlow.min.js"></script>
        <!-- 可选，将流程图导出为图片文件的扩展包 GooFlow.export.js -->
        <script type="text/javascript" src="./dist/GooFlow.export.min.js"></script>
        <!-- 可选，将流程图输出打印或另存为 PDF 的扩展包 GooFlow.print.js-->
        <script type="text/javascript" src="./dist/GooFlow.print.min.js"></script>
        <script type="text/javascript" src="./main.js"></script>
    </body>
</html>
```

然后在业务逻辑代码中调用方法：

> In file __main.js__

```javascript
    var options = {
        toolBtns: ["start round mix","end round","task","node","chat","state","plug","join","fork","complex mix"],
        haveHead: true,
        headLabel: true,
        headBtns: ["new","open","save","undo","redo","reload","print"], // 如果 haveHead = true，则定义 HEAD 区的按钮
        haveTool: true,
        haveDashed: true,
        haveGroup: true,
        useOperStack: true
    };
    var demo;
    $(document).ready(function(){
        demo = GooFlow.init("#demo",options);
        // demo = $.createGooFlow("#demo", options); // 第二种初始化方法
        demo.setNodeRemarks(remark); // remarks 为左侧工具栏按钮的 title 提示定义
        demo.loadData(jsondata); // jsondata 为表达流程图详细的 JSON 数据
    });
```

### 三：AMD 异步模式使用方法

以 _RequireJs_ 为例，先在 RequireJs 于项目的统一配置文件中加入如下设置，需要用到 `require-css` _(css.min.js)_ 插件：

> In file __require.config.js__

```javascript
requirejs.config({
    // ……
    map: {
        '+': {
            'css': 'https://cdn.bootcss.com/require-css/0.1.10/css.min.js' // https://github.com/guybedford/require-css, RequireJs plugin
        }
    },
    paths: {
        jquery: 'https://cdn.bootcss.com/jquery/1.12.4/jquery.min',
        GooFlow: 'dist/GooFlow.min',
        'GooFlow.export': 'dist/GooFlow.export.min',  // 可选，将流程图导出为图片文件的扩展包
        'GooFlow.print': 'dist/GooFlow.print.min',    // 可选，将流程图输出打印或另存为PDF的扩展包
    },

    shim:{
        'GooFlow':{
            deps:['css!../dist/GooFlow.min.css','jquery']
        }
    },
    // ……
});
```

在将会异步引入 _main.js_ 入口业务文件的 `html` 页面中，`body` 末尾加上这一段;

```html
<body>
    <div id="demo"></div>
    <!-- …… -->
    <script data-main="main.js" src="https://cdn.bootcss.com/require.js/2.3.5/require.min.js"></script>
    <script src="../assets/js/require.config.js"></script>
</body>
```

然后在具体的业务 js 文件中作包引入并初始化：

> In file __main.js__

``` javascript
require(['jquery','GooFlow'], function ( $, GooFlow ) {
    // 初始化代码
    var options = { /* …… */ };
    var demo = GooFlow.init("#demo",options);
    demo.setNodeRemarks(remark); // remarks 为左侧工具栏按钮的 title 提示定义
    demo.loadData(jsondata);     // jsondata 为表达流程图详细的 JSON 数据
});
```

如果想使用其它扩展包提供的功能，请务必保证在载入 _GooFlow.js_ 后再载入相应的扩展包，以保证相应的功能正常；

```javascript
//++ main.js 扩展功能包 ++//
require(['jquery', 'GooFlow'], function ( $, GooFlow ) {
    require(['GooFlow.export','GooFlow.print'], function (){
        // 初始化代码
        var options = { /* …… */ };
        var demo = GooFlow.init("#demo", options);
        demo.setNodeRemarks(remark);         // remarks 为左侧工具栏按钮的 title 提示定义
        demo.setHeadToolsRemarks(headBtns);  // headBtns 为顶部标题栏按钮的 title 提示设置
        demo.loadData(jsondata);             // jsondata 为表达流程图详细的 JSON 数据
        demo.onBtnSaveClick = function(){
            demo.exportDiagram(exportName);  // 流程图导出图片功能
        }
        demo.onPrintClick=function(){
            demo.print(); // 打印流程图或另存为 PDF 功能
        }
    });
});
```

### 更新历史：

+ 1.3.8:

优化代码

+ 1.3.7:

修正计算流程图实际宽高算法的一个小 bug，初始化时增加设置节点和泳道初始默认名前缀的属性。

+ 1.3.6:

修正有时选中元素时，`focus` 事件被触发两次的问题，加入右建单击绘图区空白处时，触发 `blur` 事件的操作方式；新增快捷设置某个元素的扩展业务属性的接口；新增流程图载入后再对某元素另行修改颜色或文字颜色的接口。

+ 1.3.5:

功能更新！分组泳道增加了“牛奶”色，用户可对单个节点或连线自定义特殊的图形颜色和文字颜色；设计器增加了 __Ctrl + C__ 复制节点和 __Ctrl + V__ 粘贴节点功能。

+ 1.3.4:

重要更新！修正了一些事件响应、图片导出、绘图区缩放方面的 bug，增加了椭圆、菱形、平行四边形、胶囊形 _4_ 种特殊形状的节点。

+ 1.3.3:

重要功能上线！新增对 _Bpmn 2.0_ 规范下XML格式的流程图数据的支持，可读取或输出 _Bpmn 2.0_ 规范的 XML 格式数据；允许用户以 json 文件或者 _Bpmn 2.0_ 规范的 XML 文件的方式下载流程图数据至本地。 进一步优化代码。

+ 1.3.2:

优化了代码结构和用户体验；加强内聚性，简化了流程另存为图片功能和打印功能的内部实现，摆脱了 _1.1_ 至 _1.2_ 版本中对第三方插件的依赖，导出结果更清晰，并能向后兼容到 IE9，各部分的按钮都可以通过内置接口方法进行配置；节点数据新增 `areaId` 可选属性:表示其属于哪一个区域组(泳道)。总体还加入了对 _amd、cmd_ 开发模式规范的支持。

+ 1.3:

修正一些影响用户体验和使用的 bug，增加灵活的对工作区所有操作按钮注释配置功能。

+ 1.2.1:

改善对 IE8 的支持；优化手动调整元素大小的精确度；增加对流程图打印预览或另存为 PDF 的功能。

+ 1.2:

重大更新版本！增加了对节点、连线、泳道块的右键事件和双击事件方法设定（可 `return false` 以阻止浏览器默认事件发生）；增加虚线的绘制功能方法：应众多用户的要求增加了重要的流程图缩放功能接口！可缩放范围是从原始大小的 _50%_ 至 _400%_。（仅提供方法接口，具体操作缩放的页面组件元素用户另行选择绑定第三方的）

+ 1.1:

进一步修正了 bug；增加了导出工作区内流程图为图片并让用户下载的功能，目前仅能支持 IE10 以上、Edge、Chrome、FireFox、Safari 浏览器，该功能需求载入新的 _GooFlow.export.js_ 扩展包。

+ 1.0.2:

修正了当某节点原来为 `marked`，选中再取消后 `marked` 标红样式消失的问题。

+ 1.0:

首个正式版本，相对于以前的试用版，进一步修正了 bug，可自定义颜色项更多，并且做到了不再使用一张位图，所有图标均匀为 _矢量字体_。
